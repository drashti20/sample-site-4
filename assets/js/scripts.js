var body = document.querySelector('body')
var menuTrigger = document.querySelector('#toggle-main-menu-mobile');
var menuContainer = document.querySelector('#main-menu-mobile');

menuTrigger.onclick = function() {
    menuContainer.classList.toggle('open');
    menuTrigger.classList.toggle('is-active')
    body.classList.toggle('lock-scroll')
}
function headerfixnormal(){
	
	if ($(document).width() <= 991){
				
		// ---------------- Fixed header responsive ----------------------
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > 0) {
				$('.header').addClass('fixed');
					

			} else {
				$('.header').removeClass('fixed');
			}
		});

	}
	if ($(document).width() >= 992){
				
		// ---------------- Fixed header responsive ----------------------
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > 0) {
				$('.header').addClass('fixed');
			} else {
				$('.header').removeClass('fixed');
			}
		});
		
	}
	else{
		$('.header').removeClass('fixed');
	}
	
}

jQuery(window).resize(function() {headerfixnormal();});
jQuery(document).ready(function() {headerfixnormal();});
jQuery(window).scroll(function() {headerfixnormal();});