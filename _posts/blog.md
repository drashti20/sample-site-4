---
title: 'blog'
layout: default
---

{: .full}
  {: .row}
    {: .special-title .centered-text}
      {: .icon-thumbs-up}
      <h2>Our Blog</h2>
      <p>We are passionate about web design &amp; development</p>
      <p class="shortline"></p>
    {: .spacing}
      {: .spacing}
  {: .row}
   {: .large-12 .columns}

 

  {: .post}

{: .timestamp .left .text-center}

      <span class="month">May</span>
      <span class="date">05</span>
    </div>
    <div class="info">
      <h3>
        <a href="
        #">Great quality means a lot to us</a>
      </h3>
      <p>
        By
        <span class="author">Andy Budd</span>
        |
        <a href="single.html#comments">Comments 6</a>
      </p>
    {: .content}
      <p><img src="../image/blog1.jpg" alt="computers"></p>

<p>Deleniti laboriosam soluta animi non corrupti. labore est quasi omnis ducimus. facere pariatur molestiae eum vel dignissimos debitis quam et id</p>

<p>Esse labore voluptate <a href="#">nam dolorum veniam</a> numquam et reiciendis qui itaque voluptas odit dicta et. est ullam et omnis repellat qui aliquam delectus quod aperiam quam tenetur odit illo voluptatem. optio tenetur occaecati autem vel occaecati voluptas iste sunt libero quo est ex eum. aut molestiae ea est inventore aut quia ab voluptatem hic iste</p>

<blockquote>
<p>"Provident nihil sint ducimus quasi dignissimos vero esse quisquam. Asperiores beatae facere eos corrupti ut autem natus quis cumque dolor"</p>
</blockquote>

      <a class="button" href="./title1.html">Read more</a>


  <div class="post">
    <div class="timestamp left text-center">
      <span class="month">May</span>
      <span class="date">06</span>
    </div>
    <div class="info">
      <h3>
        <a href="./title1.html">Design need to be better</a>
      </h3>
      <p>
        By
        <span class="author">Andy Budd</span>
        |
        <a href="single.html#comments">Comments 6</a>
      </p>
    </div>
    <div class="content">
      <p><img src="../image/blog2.jpg" alt="computers"></p>

<p>Deleniti laboriosam soluta animi non corrupti. labore est quasi omnis ducimus. facere pariatur molestiae eum vel dignissimos debitis quam et id</p>

<p>Esse labore voluptate <a href="#">nam dolorum veniam</a> numquam et reiciendis qui itaque voluptas odit dicta et. est ullam et omnis repellat qui aliquam delectus quod aperiam quam tenetur odit illo voluptatem. optio tenetur occaecati autem vel occaecati voluptas iste sunt libero quo est ex eum. aut molestiae ea est inventore aut quia ab voluptatem hic iste</p>

<blockquote>
<p>"Provident nihil sint ducimus quasi dignissimos vero esse quisquam. Asperiores beatae facere eos corrupti ut autem natus quis cumque dolor"</p>
</blockquote>

      <a class="button" href="./title1.html">Read more</a>
    </div>
  </div>

  <div class="post">
    <div class="timestamp left text-center">
      <span class="month">May</span>
      <span class="date">07</span>
    </div>
    <div class="info">
      <h3>
        <a href="./title1.html">This is a title for your post</a>
      </h3>
      <p>
        By
        <span class="author">Andy Budd</span>
        |
        <a href="single.html#comments">Comments 6</a>
      </p>
    </div>
    <div class="content">
      <p>Deleniti laboriosam soluta animi non corrupti. labore est quasi omnis ducimus. facere pariatur molestiae eum vel dignissimos debitis quam et id</p>

<p>Esse labore voluptate <a href="#">nam dolorum veniam</a> numquam et reiciendis qui itaque voluptas odit dicta et. est ullam et omnis repellat qui aliquam delectus quod aperiam quam tenetur odit illo voluptatem. optio tenetur occaecati autem vel occaecati voluptas iste sunt libero quo est ex eum. aut molestiae ea est inventore aut quia ab voluptatem hic iste</p>

<blockquote>
<p>"Provident nihil sint ducimus quasi dignissimos vero esse quisquam. Asperiores beatae facere eos corrupti ut autem natus quis cumque dolor"</p>
</blockquote>

      <a class="button" href="./title1.html">Read more</a>
    </div>
  </div>


      
        <div class="pager">
          
            
              <a href="/blog/page2" class="older">Older Entries</a>
            
          
          
            <span class="newer">Newer Entries</span>
          
        </div>
      

    </div>
  </div>
</div>